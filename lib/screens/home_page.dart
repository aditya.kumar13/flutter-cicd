import 'package:a_new_project_in_flutter/components/my_controllers.dart';
import 'package:a_new_project_in_flutter/utils/my_colors.dart';
import 'package:a_new_project_in_flutter/utils/my_strings.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _current = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(80.0),
        child: AppBar(
          elevation: 0.0,
          iconTheme: IconThemeData(color: MyColors.black),
          backgroundColor: Colors.white,
          title: Column(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
                child: Image.asset(
                  'images/app_logo.png',
                  scale: 0.90,
                ),
              ),
            ],
          ),
        ),
      ),
      endDrawer: Drawer(),
      body: Container(
        color: MyColors.white,
        child: Column(
          children: [
            Divider(height: 5),
            SizedBox(height: 20),
            Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                        borderSide:
                            const BorderSide(color: MyColors.searchFieldBorder),
                      ),
                      suffixIcon: IconButton(
                        onPressed: () {},
                        icon: SvgPicture.asset("images/search_icon_red.svg"),
                      ),
                      hintText: MyStrings.searchFunTone,
                      hintStyle:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                      fillColor: MyColors.searchFieldBg,
                      filled: true)),
            ),
            SizedBox(height: 20),
            CarouselSlider(
              items: [
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Container(
                    color: Colors.blue,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text('iOS'),
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text('Desktop'),
                ),
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text('Web'),
                )
              ],
              //Slider Container properties
              options: CarouselOptions(
                autoPlay: true,
                enlargeCenterPage: true,
                aspectRatio: 2.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                    print("${_current}");
                  });
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 8.0,
                  height: 8.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      /*color: _current == index
                          ? Color.fromRGBO(0, 0, 0, 0.9)
                          : Color.fromRGBO(0, 0, 0, 0.4)*/),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
