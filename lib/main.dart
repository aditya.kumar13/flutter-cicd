import 'package:a_new_project_in_flutter/screens/home_page.dart';
import 'package:a_new_project_in_flutter/screens/login_page.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:stacked_themes/stacked_themes.dart';

Future main() async {
  await ThemeManager.initialise();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final navigatorKey = GlobalKey<NavigatorState>();

  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual, overlays: [SystemUiOverlay.bottom]);
    return ThemeBuilder(
      builder: (context, regularTheme, darkTheme, themeMode) => MaterialApp(
        title: 'My Application',
        home: SplashScreen(),
        theme: regularTheme,
        darkTheme: darkTheme,
        themeMode: themeMode,
      ),
      themes: [],
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splashIconSize: 500,
      nextScreen: const HomePage(),
      splash: Image.asset(
        'images/app_logo_2x.png',
        height: double.infinity,
        width: double.infinity,
        fit: BoxFit.contain,
      ),
      duration: 3000,
      splashTransition: SplashTransition.scaleTransition,
    );
  }
}
